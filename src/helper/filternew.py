# -*- coding: utf-8 -*-

"""
Created on Wed Feb 08 11:59:19 2017

@author: Roopa

References:
https://tomroelandts.com/articles/how-to-create-a-configurable-filter-using-a-kaiser-window
https://in.mathworks.com/help/signal/ug/compensate-for-delay-and-distortion-introduced-by-filters.html
https://in.mathworks.com/help/signal/ref/filtfilt.html

        for bpf_filtfilt

Input:  signal_to_filter=signal to be filtered
        lower_cutoff=lower cutoff frequency
        higher_cutoff=higher cutoff frequency
        sampling_rate=Fs=44100
        transition_width=205 Hz

output:  filtered_signal= signal obtained after bandpass filtering
         filter_coefficient= tap/filter coefficients values
         Number_of_coefficients= number of coefficiens

         for bpf_lfilter

Input:  signal_to_filter=signal to be filtered
        lower_cutoff=lower cutoff frequency
        higher_cutoff=higher cutoff frequency
        sampling_rate=Fs=44100
        transition_width=205 Hz

output:  filtered_signal= signal obtained after bandpass filtering
         filter_coefficient= tap/filter coefficients values
         Number_of_coefficients= number of coefficiens
         t_comp= time vector with delay samples removed from end
         originalsignal_comp=original sine wave before filtering truncated by removing delay samples from end
         filtered_withoutcomp=filtered signal  with delay samples removed from beginning


"""

import scipy.io.wavfile as sci
import os
import json
import numpy as np
import math
from scipy.signal import kaiserord, filtfilt, lfilter, firwin, freqz, group_delay
import matplotlib.pyplot as plt

# Fs = 44100.0

# # transition Bandwidth from stop band to pass band BPF
# transition_width = 205
# # cutoff freuency
# lower_cutoff = 17000
# higher_cutoff = 19000

def getCorrectionValue(freq, correctionCurve, freqResoultion):
    print freq
    for i in range(len(correctionCurve)):
        if freq > correctionCurve[i][1] and freq < correctionCurve[i+1][1]:
            indexFreq = int(freq/freqResoultion - correctionCurve[i][1]/freqResoultion)

            totalIndexFreq = int((correctionCurve[i+1][1] - correctionCurve[i][1])/freqResoultion)

            return correctionCurve[i][0] + (correctionCurve[i+1][0] - correctionCurve[i+1][0])*float(indexFreq)/totalIndexFreq


# function for performing bandpass filtering using filtfilt
def bpf_filtfilt(signal_to_filter, lower_cutoff, higher_cutoff, sampling_rate, transition_width,ripple_db):
    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate transition width.
    width = transition_width / nyq_rate  # transition bandwidth 5Hz wrt nyquist rate

    # The desired attenuation in the stop band, in dB.
    # ripple_db = 60.0  # stop band attenuation

    # Design a Kaiser window to limit ripple and width of transition region
    N, beta = kaiserord(ripple_db, width)
    # print "BETA FROM KAISER BPF",beta
    # print "NUMBER OF TAPS FROM KAISER BPF", N

    # The cutoff frequency of the filter.
    lower_cutoff_hz = lower_cutoff
    high_cutoff_hz = higher_cutoff

    # Use firwin with a Kaiserz window to create a band pass FIR filter.
    tapsName = str(lower_cutoff_hz) + "to" + str(high_cutoff_hz) + "_" + str(transition_width) + "_" + \
               str(ripple_db) + ".npy"

    print tapsName

    if os.path.isfile(tapsName):
        taps_data = np.load(tapsName)
        taps = taps_data[0]
        delay = taps_data[1]
    else:
        taps = firwin(N, [lower_cutoff_hz / nyq_rate, high_cutoff_hz / nyq_rate], window=('kaiser', beta), pass_zero=False)
        print "Saving Taps to " + tapsName

        # DELAY CALCULATION
        w, gd = group_delay((taps, 1.0))
        delay = math.ceil(np.mean(gd))

        taps_data = np.array([taps,delay])
        np.save(tapsName, taps_data)

    # zero phase digital filter using filtfilt command pad delay number of zeros at end of signal before filtering using padlen=int(delay)
    filtered_by_filtfilt = filtfilt(taps, 1.0, signal_to_filter, padlen=int(delay))
    return filtered_by_filtfilt

def hpf_filtfilt(signal_to_filter, higher_cutoff, sampling_rate, transition_width,ripple_db):
    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate transition width.
    width = transition_width / nyq_rate  # transition bandwidth 5Hz wrt nyquist rate

    # The desired attenuation in the stop band, in dB.
    # ripple_db = 60.0  # stop band attenuation

    # Design a Kaiser window to limit ripple and width of transition region
    N, beta = kaiserord(ripple_db, width)
    # print "BETA FROM KAISER BPF",beta
    # print "NUMBER OF TAPS FROM KAISER BPF", N

    #to solve
    # filter with an even number of coefficients must have zero response at the Nyquist frequency.

    if(N%2 == 0):
        N += 1

    # The cutoff frequency of the filter.
    high_cutoff_hz = higher_cutoff

    # Use firwin with a Kaiserz window to create a band pass FIR filter.
    tapsName = "highPass" + str(high_cutoff_hz) + "_" + str(transition_width) + "_" + \
               str(ripple_db) + ".npy"

    print tapsName

    if os.path.isfile(tapsName):
        taps_data = np.load(tapsName)
        taps = taps_data[0]
        delay = taps_data[1]
    else:
        taps = firwin(N, high_cutoff_hz / nyq_rate, window=('kaiser', beta), pass_zero=False)
        print "Saving Taps to " + tapsName

        # DELAY CALCULATION
        w, gd = group_delay((taps, 1.0))
        delay = math.ceil(np.mean(gd))

        taps_data = np.array([taps,delay])
        np.save(tapsName, taps_data)

    # zero phase digital filter using filtfilt command pad delay number of zeros at end of signal before filtering using padlen=int(delay)
    filtered_by_filtfilt = filtfilt(taps, 1.0, signal_to_filter, padlen=int(delay))
    return filtered_by_filtfilt

def chirpCorrection_BPF_filtfilt(signal_to_filter, lower_cutoff, higher_cutoff, sampling_rate, nFFTPreamble,
                                 transition_width, ripple_db, correctionCurve, alpha1, tau1):
    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate transition width.
    width = transition_width / nyq_rate  # transition bandwidth 5Hz wrt nyquist rate

    # The desired attenuation in the stop band, in dB.
    # ripple_db = 60.0  # stop band attenuation

    # Design a Kaiser window to limit ripple and width of transition region
    N, beta = kaiserord(ripple_db, width)
    # print "BETA FROM KAISER BPF",beta
    # print "NUMBER OF TAPS FROM KAISER BPF", N

    # The cutoff frequency of the filter.
    lower_cutoff_hz = lower_cutoff
    high_cutoff_hz = higher_cutoff

    # Use firwin with a Kaiserz window to create a band pass FIR filter.
    tapsName = str(lower_cutoff_hz) + "to" + str(high_cutoff_hz) + "_" + str(transition_width) + "_" + \
               str(ripple_db) + ".npy"

    print tapsName

    if os.path.isfile(tapsName):
        taps_data = np.load(tapsName)
        taps = taps_data[0]
        delay = taps_data[1]
    else:
        taps = firwin(N, [lower_cutoff_hz / nyq_rate, high_cutoff_hz / nyq_rate], window=('kaiser', beta), pass_zero=False)
        print "Saving Taps to " + tapsName

        # DELAY CALCULATION
        w, gd = group_delay((taps, 1.0))
        delay = math.ceil(np.mean(gd))

        taps_data = np.array([taps,delay])
        np.save(tapsName, taps_data)

    print "Length of the correction curve :", len(correctionCurve[1])

    if len(correctionCurve) > 0:

        modifiedTapsName = "modified" + tapsName

        increaseFactor = 2**4
        nFFTmodfiedTaps = nFFTPreamble*increaseFactor

        FFTTaps = np.fft.fft(taps, n=nFFTmodfiedTaps)

        FFTTapsNyq = FFTTaps[:nFFTmodfiedTaps/2]
        FFTfreqNyq = np.linspace(0, sampling_rate/2, len(FFTTaps)/2)

        correctionCurveIndex = 0
        fftIndex = 0

        for i in xrange(len(FFTfreqNyq)):
            if correctionCurveIndex >= len(correctionCurve[1])-1:
                break
            if FFTfreqNyq[i] > correctionCurve[0][0] and FFTfreqNyq[i] < correctionCurve[0][1]:
                FFTTapsNyq[i] *= correctionCurve[1][correctionCurveIndex] + \
                                 (correctionCurve[1][correctionCurveIndex+1]-correctionCurve[1][correctionCurveIndex])*\
                                 float(fftIndex%increaseFactor)/increaseFactor
                fftIndex += 1
                if fftIndex%increaseFactor == 0:
                    correctionCurveIndex += 1

        # FFTTapsNyq /= ((1+alpha1*np.exp(-2*1j*np.pi*FFTfreqNyq*tau1)))

        modifiedFFTTaps = np.append(FFTTapsNyq, FFTTapsNyq[::-1])

        modiFiedTaps = np.real(np.fft.ifft(modifiedFFTTaps, n=nFFTmodfiedTaps)[:len(taps)])

        # plt.figure("Taps comparision")
        #
        # plt.subplot(211)
        # plt.plot(taps)
        #
        # plt.subplot(212)
        # plt.plot(modiFiedTaps)

        # plt.figure("FFT of modified taps")
        # plt.plot(FFTfreqNyq, abs(np.fft.fft(modiFiedTaps,n=nFFTmodfiedTaps)[:nFFTmodfiedTaps/2]))

    # zero phase digital filter using filtfilt command pad delay number of zeros at end of signal before filtering using padlen=int(delay)
    filtered_by_filtfilt = filtfilt(modiFiedTaps, 1.0, signal_to_filter, padlen=int(delay))
    return filtered_by_filtfilt

# function for performing bandpass filtering using lfilter
def bpf_lfilter(signal_to_filter, lower_cutoff, higher_cutoff, sampling_rate, transition_width):

    nsamples = len(signal_to_filter)
    t = np.arange(nsamples) / Fs

    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate transition width.
    width = transition_width / nyq_rate  # transition bandwidth 5Hz wrt nyquist rate

    # The desired attenuation in the stop band, in dB.
    ripple_db = 60.0  # stop band attenuation

    # Design a Kaiser window to limit ripple and width of transition region
    N, beta = kaiserord(ripple_db, width)

    # The cutoff frequency of the filter.
    lower_cutoff_hz = lower_cutoff
    high_cutoff_hz = higher_cutoff

    # Use firwin with a Kaiser window to create a band pass FIR filter.
    taps = firwin(N, [lower_cutoff_hz / nyq_rate, high_cutoff_hz / nyq_rate], window=('kaiser', beta), pass_zero=False)
    filtered_x = lfilter(taps, 1.0, signal_to_filter)

    # DELAY CALCULATION
    w, gd = group_delay((taps, 1.0))
    delay = math.ceil(np.mean(gd))

    # DELAY COMPENSATION
    t_comp = t[0:int(-delay)]
    originalsignal_comp = signal_to_filter[0:int(-delay)]

    # Shift the filtered signal by removing its first delay samples
    kaiser_bpf_signal_compensated = np.delete(filtered_x, np.s_[int(0):int(delay)], 0)

    return kaiser_bpf_signal_compensated, taps, N, t_comp, originalsignal_comp, filtered_x

if __name__ == "__main__":
    # function call for filtfilt

    fc = 18e3

    signalLength = 1000

    lower_cutoff = 16000
    higher_cutoff = 20050
    Fs = 44.1e3

    ts = 1/Fs

    t = np.arange(0, signalLength*ts, ts)

    signal = np.sin(2*np.pi*fc*t)

    transition_width = 200
    rippleDB = 90
    filtered_signal = bpf_filtfilt(signal, lower_cutoff,higher_cutoff, Fs, transition_width, rippleDB)


    # filtered_signal, filter_coefficient, Number_of_coefficients, t_comp, originalsignal_comp, filtered_withoutcomp = bpf_lfilter(
    #     signal_for_filtering, lower_cutoff, higher_cutoff, Fs, transition_width)



