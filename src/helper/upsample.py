'''

Created on Wed Nov 28 2018


'''

import numpy as np

def upsample(arr, up_sample_factor):
    le = len(arr)
    y = np.zeros((up_sample_factor - 1, le))  # N-1 zeros inserted between two samples
    c = np.vstack((y, arr))  # concatenation vertically
    final = c.transpose()
    n = final.size
    array_up = np.reshape(final, n)
    c = np.concatenate((array_up, np.zeros(up_sample_factor - 1)))
    return c
