# Created on Wed Feb 08 11:59:19 2017
#
# @author: Roopa
#
# References:
# https://tomroelandts.com/articles/how-to-create-a-configurable-filter-using-a-kaiser-window
# https://in.mathworks.com/help/signal/ug/compensate-for-delay-and-distortion-introduced-by-filters.html
# https://in.mathworks.com/help/signal/ref/filtfilt.html
#
#
# Input:  signal_to_filter=signal to be filtered
#         lower_cutoff=lower cutoff frequency=Fbit*3 , Fbit=Float(Fs)/Float(T_b) --->bit frequency
#         sampling_rate=Fs=44100
#         N=100 filter order
#
#
# output:  filtered_by_filtfilt=output of zero phase filtering [FILTFILT function is used]

import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.signal import kaiserord, filtfilt, firwin, freqz, group_delay
import os

# function for performing bandpass filtering
def Lpf_filtfilt(signal_to_filter, lower_cutoff, sampling_rate,N):
    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0
    # Use firwin with a hamming window to create a low pass FIR filter.

    # N = 100
    # taps = firwin(N, lower_cutoff / nyq_rate, window=('kaiser', 8), pass_zero=True)

    tapsName = "Lowpass" + str(lower_cutoff) + "_" + str(N) + ".npy"
    print tapsName

    if os.path.isfile(tapsName):
        taps_data = np.load(tapsName)
        taps = taps_data[0]
        delay = taps_data[1]
    else:
        taps = firwin(N, lower_cutoff / nyq_rate, window='hamming', pass_zero=True)
        print "Saving Taps to " + tapsName

        # DELAY CALCULATION
        w, gd = group_delay((taps, 1.0))
        delay = math.ceil(np.mean(gd))

        taps_data = np.array([taps,delay])
        np.save(tapsName, taps_data)
        w, h = freqz(taps)
    # plt.figure()
    # plt.plot(w, 20 * np.log10(abs(h)), 'b')
    # plt.ylabel('Amplitude [dB]', color='b')
    # plt.xlabel('Frequency [rad/sample]')
    # print 'no of taps for lpf', len(taps)

    signal_zero_padded = []
    # check for the delay length vrs input array size as error occurs if size of input array is less then delay size [length of input vector should be atleast padlen]
    if len(signal_to_filter) < int(delay):
        zeros_to_add = int(delay) - len(signal_to_filter)
        zeros_pad = np.zeros([int(zeros_to_add + 1)])
        #        print 'length of zeors',len(zeros_pad)
        signal_zero_padded = np.concatenate([signal_to_filter, zeros_pad])
    else:
        signal_zero_padded = signal_to_filter

    # print 'length of zeropadded signal=',len(signal_zero_padded)
    # zero phase digital filter using filtfilt command pad delay number of zeros at end of signal before filtering using padlen=int(delay)

    filtered_by_filtfilt = filtfilt(taps, 1.0, signal_zero_padded, padlen=int(delay))

    # as the input array is increased in size after filering padded zeros are removed

    filtered_by_filtfilt = filtered_by_filtfilt[0:len(signal_to_filter)]
    #    print 'length of bpf signal=',len(filtered_by_filtfilt)
    return filtered_by_filtfilt