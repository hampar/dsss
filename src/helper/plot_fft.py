"""
Created on Mon Nov 07 11:42:14 2016

@author: Dushyanth Kumar
 Purpose  - The script takes a signnal array and Sampling frequency and plots the Freq spectrum
 Parameters
     received_wave : Received wave from the transmitter
     Fs : Sampling Frequency
     **kwargs : allows you to pass keyworded variable length
                of arguments to a function.
      Like Title, Xlabel, Ylabel of the plot, to save plot with title tag
	 and you can also print name, company, address ...etc.,
      by passing arguements to the function
	  Eg if user wants to save the file keyword should be  save ="yes"
      Eg of excuting the code
      plot_fft(received_wave.ys, Fs, title="FFT_PLOT", xlabel="freq(Hz)", ylabel="Amp(m)", save="yes")
 Output
      FFT plot with assigned title and labels,
      Calculates first, second and third highest frequencies,
      image file is saved with given title and
      prints user arguements passed to the function
"""

from scipy.io.wavfile import read
import numpy as np
import matplotlib.pyplot as plt

#plt.close('all')
def plot_fft(received_wave, Fs, color,**kwargs):
    size1=len(received_wave)
    print "Fs :", Fs
    # size1=1024
    Af= np.fft.fft(received_wave,size1)    #Calculating the fft coeff
    Amp=np.sqrt(np.abs(Af))          #calculation the amp sqrt(x^2+y^2)
    freq=np.linspace(0,Fs,size1)     # creating a freq array based on sampling freq Fs and size
    Amp_hf=Amp[0:int(size1/2)]        # HAlfind the amplitude array rejecting freq Fs/2 to Fs
    Phas=np.angle(Af)
    Phas_hf=Phas[0:int(size1/2)]
    freq_hf=freq[0:int(size1/2)]
    idx=np.argsort(Amp_hf)            #This shorts the  index of the array in acending order
    freq1=((idx[-1])/float(size1))*Fs # freq1 is the maximum freq freq2 second maximum and so on
    # print('Maximum Freq:', freq1)
    # freq2=((idx[-2])/float(size1))*Fs
    # print('Second Maximum Freq:', freq2)
    # freq3=((idx[-3])/float(size1))*Fs
    # print('Third Maximum Freq:', freq3)
    plt.figure()
    plt.plot(freq_hf, Amp_hf,color)
    plt.text(freq1, max(Amp_hf),str(int(freq1)))
    # plt.text(freq2, max(Amp_hf),str(int(freq2)))
    plt.xlabel("Frequency Hz")
    plt.ylabel("magnitude")
    # plt.xlim([-1000,19000])
    # plt.title("fft plot")
    # plt.show()

    for key, value in kwargs.iteritems():
        if(key == 'title1'):
            plt.title(value)
            z=value
        if(key == 'xlabel'):
            plt.xlabel(value)
        if(key == 'ylabel'):
            plt.ylabel(value)
        if(key == 'save'):
            if(value == 'yes'):
                plt.savefig(z+'.png')
        else:
            print ("%s == %s" %(key,value))

    return True
    # return Af,freq_hf
    # return freq_hf, Amp_hf,Phas_hf,Af

def plot_fft_withN(received_wave, Fs, color,N,**kwargs):
    size1=N
    print "Fs :", Fs
    # size1=1024
    Af= np.fft.fft(received_wave,size1)    #Calculating the fft coeff
    Amp=np.sqrt(np.abs(Af))          #calculation the amp sqrt(x^2+y^2)
    freq=np.linspace(0,Fs,size1)     # creating a freq array based on sampling freq Fs and size
    Amp_hf=Amp[0:int(size1/2)]        # HAlfind the amplitude array rejecting freq Fs/2 to Fs
    Phas=np.angle(Af)
    Phas_hf=Phas[0:int(size1/2)]
    freq_hf=freq[0:int(size1/2)]
    idx=np.argsort(Amp_hf)            #This shorts the  index of the array in acending order
    freq1=((idx[-1])/float(size1))*Fs # freq1 is the maximum freq freq2 second maximum and so on
    # print('Maximum Freq:', freq1)
    # freq2=((idx[-2])/float(size1))*Fs
    # print('Second Maximum Freq:', freq2)
    # freq3=((idx[-3])/float(size1))*Fs
    # print('Third Maximum Freq:', freq3)
    plt.figure()
    plt.plot(freq_hf, Amp_hf,color)
    plt.text(freq1, max(Amp_hf),str(int(freq1)))
    # plt.text(freq2, max(Amp_hf),str(int(freq2)))
    plt.xlabel("Frequency Hz")
    plt.ylabel("magnitude")
    # plt.xlim([-1000,19000])
    # plt.title("fft plot")
    # plt.show()

def plot_multiple_fft(received_waveArray, Fs, colorsArray, legendArray,**kwargs):
    for received_wave,color,legend in zip(received_waveArray, colorsArray,legendArray):
        plt.figure(legend)
        size1=len(received_wave)
        print "Fs :", Fs
        # size1=1024
        Af= np.fft.fft(received_wave,size1)    #Calculating the fft coeff
        Amp=np.sqrt(np.abs(Af))          #calculation the amp sqrt(x^2+y^2)
        freq=np.linspace(0,Fs,size1)     # creating a freq array based on sampling freq Fs and size
        Amp_hf=Amp[0:int(size1/2)]        # HAlfind the amplitude array rejecting freq Fs/2 to Fs
        Phas=np.angle(Af)
        Phas_hf=Phas[0:int(size1/2)]
        freq_hf=freq[0:int(size1/2)]
        idx=np.argsort(Amp_hf)            #This shorts the  index of the array in acending order
        freq1=((idx[-1])/float(size1))*Fs # freq1 is the maximum freq freq2 second maximum and so on
        # print('Maximum Freq:', freq1)
        # freq2=((idx[-2])/float(size1))*Fs
        # print('Second Maximum Freq:', freq2)
        # freq3=((idx[-3])/float(size1))*Fs
        # print('Third Maximum Freq:', freq3)
        plt.plot(freq_hf, Amp_hf,color)
        plt.text(freq1, max(Amp_hf),str(int(freq1)))
        # plt.text(freq2, max(Amp_hf),str(int(freq2)))
        plt.xlabel("Frequency Hz")
        plt.ylabel("magnitude")
        # plt.xlim([-1000,19000])
        # plt.title("fft plot")
        # plt.show()
    plt.legend(legendArray)


if __name__ == "__main__":
    # mod_signal = read("bit_by_bit_2bit_freq_shift.wav") #recorded signal for filtering
    # received_wave=np.array( mod_signal[1],dtype=float)
    Fs=44100
    # plot_fft(received_wave, Fs, title="bit_by_bit_36frames", xlabel="freq(Hz)", ylabel="Amp(m)", save="no")

