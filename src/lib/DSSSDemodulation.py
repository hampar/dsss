import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import max_len_seq
from src.helper.plot_fft import plot_fft, plot_fft_withN, plot_multiple_fft
from src.helper.lowPassFilter import Lpf_filtfilt
from src.helper.filternew import bpf_filtfilt, hpf_filtfilt
from scipy.io import wavfile
import json

fs = 44100
# fs = 48000
ts = 1./fs

fc = 17000

# Nb = 2540
Nb = 2619
Tb = Nb*ts

# Ng = 1000
Ng = 0

fDelta = 2/Tb
amplitude = 0.5

d = lambda t,k:np.sin(2*np.pi*(4+k)*t/Tb)

numBits = 33

cSeq, _ = max_len_seq(7)

cSeq = 2 * np.array(cSeq) - 1

chipUpsampleFactor = 20

# chipSequence = np.repeat(cSeq, chipUpsampleFactor)
chipSequence = np.array([])
for i in cSeq:
    chipSequence = np.append(chipSequence, i)
    chipSequence = np.append(chipSequence, np.zeros(chipUpsampleFactor-1))

tSinc = np.arange(-4, 4, 0.1)

sinc = np.sinc(tSinc)

sinc /= np.sqrt(np.mean(sinc**2))

# plot_fft_withN(sinc, fs, 'g', 4096)

chipSequence = np.convolve(chipSequence, sinc)

# sincArray = np.arange(-4,4,0.1)
#
# sinc = np.sinc(sincArray)

t = np.arange(0, Nb*ts, ts)

chipSequenceTotal = np.tile(chipSequence, numBits)

# sincInterpolatedchipSequence = np.convolve(chipSequenceTotal, sinc, "full")

# sincInterpolatedChip = np.convolve(chipSequence, sinc)

def peakDetect(corrOut):
    peakIndex = np.argmax(corrOut)
    print 'PEAK :', peakIndex
    peakFirst = max(corrOut)
    peakRatio = 0.8

    peak = peakFirst

    bitCount = 0

    # while(peak > peakRatio*peakFirst):
    #     tempPeakIndex = peakIndex - (Nb+Ng)
    #     tempCorrArray = corrOut[tempPeakIndex-30:tempPeakIndex:30]
    #     temppeak = max(tempCorrArray)
    #     peakIndex = tempPeakIndex - 30 + np.argmax(tempCorrArray)
    #     if peakIndex < 0:
    #         peak = peak
    #         peakIndex = tempPeakIndex + (Nb+Ng)
    #         break
    #     else:
    #         peak = temppeak
    # return peakIndex - 1309
    for i in range(len(corrOut)):
        if corrOut[i] > peakRatio*peak:
            print i
            # return np.argmax(corrOut[i:i + 30]) + i - 1309 - 6 * (Nb + Ng) #for recordedJSONFIle29
            # print "max significant peak :", np.argmax(corrOut[i:i + 30]) + i - 1309
            dataStarttemp = np.argmax(corrOut[i:i + 30]) + i - 1309
            # dataStarttemp = np.argmax(corrOut[i:i + 30]) + i - 1309 - 3*(Nb+Ng) #for recordedJSONFIle38
            dataStarttemp = np.argmax(corrOut[i:i + 30]) + i - 1309 - (Nb+Ng) #for recordedJSONFIle33
            print "data start temp :", dataStarttemp
            return dataStarttemp
            # return peak- (Nb+Ng)
    # return 0

syntheticFile = "/home/harshaampar/PycharmProjects/DSSS/src/lib/spreadSignal.wav"

recordedJSONFileName = "/home/harshaampar/Downloads/1553018907308_12_0.json"

recordedJSONFileName1 = "/home/harshaampar/Downloads/1553060507416_12_0.json"
recordedJSONFileName2 = "/home/harshaampar/Downloads/1553060521255_12_0.json"
recordedJSONFileName3 = "/home/harshaampar/Downloads/1553069662632_12_0.json"
recordedJSONFileName4 = "/home/harshaampar/Downloads/1553114038103_12_0.json"
recordedJSONFileName5 = "/home/harshaampar/Downloads/1553149834016_12_0.json"
recordedJSONFileName6 = "/home/harshaampar/Downloads/1553151029049_12_0.json"
recordedJSONFileName7 = "/home/harshaampar/Downloads/1553186095000_12_0.json"
recordedJSONFileName8 = "/home/harshaampar/Downloads/1553420395381_12_.json"
recordedJSONFileName9 = "/home/harshaampar/Downloads/1553500921911_12_.json"
recordedJSONFileName10 = "/home/harshaampar/Downloads/1553501943361_12_.json"
recordedJSONFileName11 = "/home/harshaampar/Downloads/1553502246217_12_.json"
recordedJSONFileName12 = "/home/harshaampar/Downloads/1553502434542_12_.json"
recordedJSONFileName13 = "/home/harshaampar/Downloads/1553502642120_12_.json"
recordedJSONFileName14 = "/home/harshaampar/Downloads/1553506053058_12_.json"
recordedJSONFileName15 = "/home/harshaampar/Downloads/1553506287645_12_.json"
recordedJSONFileName16 = "/home/harshaampar/Downloads/1553506765209_12_.json"
recordedJSONFileName17 = "/home/harshaampar/Downloads/1553510327085_12_.json"
recordedJSONFileName18 = "/home/harshaampar/Downloads/1553510479940_12_.json"
recordedJSONFileName19 = "/home/harshaampar/Downloads/1553510724099_12_.json"
recordedJSONFileName20 = "/home/harshaampar/Downloads/1553510892261_12_.json"
recordedJSONFileName21 = "/home/harshaampar/Downloads/1553511158797_12_.json"
recordedJSONFileName22 = "/home/harshaampar/Downloads/1553511679154_12_.json"
recordedJSONFileName23 = "/home/harshaampar/Downloads/1553511883555_12_.json"
recordedJSONFileName24 = "/home/harshaampar/Downloads/1553512107143_12_.json"
recordedJSONFileName25 = "/home/harshaampar/Downloads/1553512211796_12_.json"
recordedJSONFileName26 = "/home/harshaampar/Downloads/1553512463954_12_.json"
recordedJSONFileName27 = "/home/harshaampar/Downloads/1553533525699_12_.json"
recordedJSONFileName28 = "/home/harshaampar/Downloads/1553533554302_12_.json"
recordedJSONFileName29 = "/home/harshaampar/Downloads/1553533831157_12_.json"
recordedJSONFileName30 = "/home/harshaampar/Downloads/1553533806675_12_.json"
recordedJSONFileName31 = "/home/harshaampar/Downloads/1553535864345_12_.json"
recordedJSONFileName32 = "/home/harshaampar/Downloads/1553577060661_12_.json"
recordedJSONFileName33 = "/home/harshaampar/Downloads/1553581652268_12_.json"
recordedJSONFileName34 = "/home/harshaampar/Downloads/1553592899292_12_.json"
recordedJSONFileName35 = "/home/harshaampar/Downloads/1553593113478_12_.json"
recordedJSONFileName36 = "/home/harshaampar/Downloads/1553593273396_12_.json"
recordedJSONFileName37 = "/home/harshaampar/Downloads/1553593440975_12_.json"
recordedJSONFileName38 = "/home/harshaampar/Downloads/1553593399535_12_.json"

recordedWavFileName1 = "/home/harshaampar/Downloads/default_26.wav"

# inputFileName = recordedJSONFileName38
inputFileName = recordedWavFileName1
# inputFileName = syntheticFile
#
# REC_TYPE = "JSON"
REC_TYPE = "WAV"

if REC_TYPE is "JSON":
    inputSignal = json.load(open(inputFileName))['audio_chunks']
else: #if REC_TYPE is "WAV":
    fs_, inputSignal = wavfile.read(inputFileName)
    print "Sampling freq :", fs_

# inputSignal = hpf_filtfilt(inputSignal, 16500, fs, 250, 60)
#
# inputSignalMax = max(abs(inputSignal))
#
# for i in range(len(inputSignal)):
#     if abs(inputSignal[i]) > 0.1*inputSignalMax:
#         dataStartRough = i
#         break

# inputSignal = inputSignal[dataStartRough-1000:]

Ntrec = len(inputSignal)
tCarrierRec = np.arange(0, Ntrec*ts, ts)
tCarrierRec = tCarrierRec[:Ntrec]

# carrierRec = np.sin(2*np.pi*fc*tCarrierRec)
carrierRec = np.exp(1j*2*np.pi*fc*tCarrierRec)

plot_fft(inputSignal, fs, 'y')
plot_fft(carrierRec, fs, 'y')

downConvertedOut = inputSignal*carrierRec

# plot_fft(downConvertedOut, fs, 'y')

Tc = chipUpsampleFactor*ts

fChip = 1/Tc

# fCutoff = fChip/2
fCutoff = 5000
# fCutoff = fcy

print "Chip frequency :", fChip, "Chip cutoff :", fCutoff
downConvertedOut = Lpf_filtfilt(downConvertedOut, fCutoff, fs, 60)
# downConvertedOut = bpf_filtfilt(downConvertedOut, 200, fCutoff, fs, 50, 60)

print "Length of down converted signal:", len(downConvertedOut)

# for i in range(len(downConvertedOut)):
#     if abs(downConvertedOut[i]) > 0.5*(max(abs(downConvertedOut))):
#         downConvertedOut = downConvertedOut[i-1000:]
#         break

# correlateSamples = 22000
correlateSamples = 48200
# correlateSamples = 20000

# corrOut = np.correlate(downConvertedOut[:correlateSamples], chipSequence, "same")
corrOut = abs(np.correlate(downConvertedOut[:correlateSamples], chipSequence, "same"))

dataStart = peakDetect(corrOut)
if dataStart < 0:
    downConvertedOut = np.append(np.zeros(-dataStart), downConvertedOut)
    dataStart = 0
# dataStart = 34568
print "data start:", dataStart

# dataStart = 0
dataEnd = dataStart + numBits*(Nb+Ng)
print "data end:", dataEnd

plt.figure("Down converted out")
plt.plot(corrOut)
plt.show()

# basebandData = inputSignal[dataStart:dataEnd]
basebandData = downConvertedOut[dataStart:dataEnd]

print 'len baseband data:', len(basebandData)

# symbolIndexArray = [0,1,2,3,4,5,6,7,8,9]
symbolIndexArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
numSymbols = len(symbolIndexArray)
symbolArray = []

symbolFreqArray = []

for i in range(numSymbols):
    symbolArray.append(d(t, symbolIndexArray[i]))
    symbolFreqArray.append((symbolIndexArray[i]+4)/Tb)
    print "Base band freq of first symbol:", symbolFreqArray[i]

# symbolIndex1 = 11
# symbolIndex2 = 12
# symbolIndex3 = 13
#
# symbol1 = d(t, symbolIndex1)
# symbol2 = d(t, symbolIndex2)
# symbol3 = d(t, symbolIndex3)

# symbol1 = np.append(symbol1, np.zeros(Ng))
# symbol2 = np.append(symbol2, np.zeros(Ng))

print "Length of baseband signal:", len(basebandData)

# despreadSignal = basebandData*np.tile(np.append(chipSequence, np.zeros(Ng)), numBits)

# despreadSignal -= 1

# corrSymbol1 = np.correlate(despreadSignal, symbol1, "same")
# corrSymbol2 = np.correlate(despreadSignal, symbol2, "same")

corrSymbol1 = np.array([])
corrSymbol2 = np.array([])
corrSymbol3 = np.array([])

print "Correlation out length:", len(corrSymbol1)

detectedSymbol = []

bitCorrTotal1 = np.array([])
bitCorrTotal2 = np.array([])
bitCorrTotal3 = np.array([])

cumsumTotal = []
# cumsumTotal1 = np.array([])
# cumsumTotal2 = np.array([])
# cumsumTotal3 = np.array([])

despreadSignal = np.array([])
avgdespreadSignal = np.array([])
avgdespreadSignal1 = np.array([])

avgStandardValue = 0

confidenceArray = np.array([])

# chipSequence = np.append(chipSequence, np.zeros(Ng))

for i in range(numBits):
    bitStart = i*(Nb+Ng)
    bitEnd = (i+1)*(Nb+Ng) - Ng
    # bitEnd = (i+1)*(Nb+Ng)

    print "bit start, bit end :", bitStart, bitEnd

    bitBaseBandData = basebandData[bitStart:bitEnd]

    # avgBitspreaddata = np.mean(bitBaseBandData)
    # bitBaseBandData -= avgBitspreaddata
    # bitBaseBandData /= np.sqrt(np.mean(bitBaseBandData**2))
    #
    # bitBaseBandData = np.convolve(bitBaseBandData, sinc, "same")
    # bitBaseBandData = Lpf_filtfilt(bitBaseBandData, fCutoff, fs, 100)

    bitdeSpreadData = bitBaseBandData*chipSequence

    # bitdeSpreadData = np.cumsum(bitdeSpreadData)


    # bitdeSpreadData -= 1
    avgBitspreaddata1 = np.mean(bitdeSpreadData)

    # if avgStandardValue == 0:
    #     avgStandardValue = avgBitspreaddata1
    # else:
    #     bitdeSpreadData *= (avgStandardValue/avgBitspreaddata1)

    # avgdespreadSignal = np.append(avgdespreadSignal, avgBitspreaddata)
    avgdespreadSignal1 = np.append(avgdespreadSignal1, avgBitspreaddata1)

    # bitdeSpreadData = np.convolve(bitdeSpreadData, sinc, "same")

    # downconvertedBit = bitdeSpreadData*carrierRec[bitStart:bitEnd]

    despreadSignal = np.append(despreadSignal, bitdeSpreadData)

    # despreadSignal = np.cumsum(despreadSignal)

    # symbol1Decision = np.real(symbol1*bitdeSpreadData)
    # symbol2Decision = np.real(symbol2*bitdeSpreadData)

    # symbol1Decision = symbol1*bitdeSpreadData
    # symbol2Decision = symbol2*bitdeSpreadData
    # symbol3Decision = symbol3*bitdeSpreadData

    symbolDecision = []
    symbolScore = []
    symbolscorecumsum = []

    for i in range(numSymbols):
        symbolDecision.append(symbolArray[i]*bitdeSpreadData)
        symbolScore.append(abs(sum(symbolDecision[i])))
        symbolscorecumsum.append(np.cumsum(symbolDecision[i]))

    # bitCorrTotal1 = np.append(bitCorrTotal1, symbol1Decision)
    # bitCorrTotal2 = np.append(bitCorrTotal2, symbol2Decision)
    # bitCorrTotal3 = np.append(bitCorrTotal3, symbol2Decision)

    # symbol1Score = abs(sum(symbol1Decision))
    # symbol2Score = abs(sum(symbol2Decision))
    # symbol3Score = abs(sum(symbol3Decision))

    # print "Symbol scores:",symbol1Score, symbol2Score, symbol3Score

    # symbolscorecumsum1 = ((symbol1Decision))
    # symbolscorecumsum2 = ((symbol2Decision))
    #
    # symbolscorecumsum1 = np.cumsum((symbol1Decision))
    # symbolscorecumsum2 = np.cumsum((symbol2Decision))
    # symbolscorecumsum3 = np.cumsum((symbol3Decision))

    # cumsumTotal1 = np.append(cumsumTotal1, symbolscorecumsum1)
    # cumsumTotal2 = np.append(cumsumTotal2, symbolscorecumsum2)
    # cumsumTotal3 = np.append(cumsumTotal3, symbolscorecumsum3)

    # bitCorr1 = np.correlate(bitdeSpreadData, symbol1, "same")
    # bitCorr2 = np.correlate(bitdeSpreadData, symbol2, "same")
    # bitCorr3 = np.correlate(bitdeSpreadData, symbol3, "same")
    #
    # corrSymbol1 = np.append(corrSymbol1, bitCorr1)
    # corrSymbol2 = np.append(corrSymbol2, bitCorr2)
    # corrSymbol3 = np.append(corrSymbol3, bitCorr2)

    # if sum(abs(corrSymbol1[bitStart:bitEnd])) > sum(abs(corrSymbol2[bitStart:bitEnd])):
    # if max(abs(corrSymbol1[bitStart:bitEnd])) > max(abs(corrSymbol2[bitStart:bitEnd])):
    # if max(abs(bitCorr1)) > max(abs(bitCorr2)):
    # if symbol1Score > symbol2Score:
    #     symbolGuessed = 0
    # else:
    #     symbolGuessed = 1
    # maxSymbolDecision = max(symbol1Score, symbol2Score, symbol3Score)

    maxSymbolDecision = max(symbolScore)

    # if symbol1Score == maxSymbolDecision:
    #     confidence = 1 - max(symbol2Score, symbol3Score)/float(maxSymbolDecision)
    #     confidenceArray = np.append(confidenceArray, 100*confidence)
    #     symbolGuessed = 0
    # elif symbol2Score == maxSymbolDecision:
    #     confidence = 1 - max(symbol1Score, symbol3Score)/float(maxSymbolDecision)
    #     confidenceArray = np.append(confidenceArray, 100*confidence)
    #     symbolGuessed = 1
    # else: #symbol3Score == maxSymbolDecision:
    #     confidence = 1 - max(symbol1Score, symbol2Score)/float(maxSymbolDecision)
    #     confidenceArray = np.append(confidenceArray, 100*confidence)
    #     symbolGuessed = 2
    symbolGuessed = symbolIndexArray[np.argmax(symbolScore)]
    symbolScore.remove(maxSymbolDecision)
    confidence = 1 - max(symbolScore)/float(maxSymbolDecision)
    confidenceArray = np.append(confidenceArray, 100*confidence)
    # print "symbolGuessed", symbolGuessed
    detectedSymbol.append(symbolGuessed)

sentSymbols = np.load("dataBits.npy")

print sentSymbols

print np.array(detectedSymbol)

errors = 0

for i in range(numBits):
    if sentSymbols[i] != detectedSymbol[i]:
        errors+=1

print "Total errors : ",errors

plt.figure("Downconverted out")
plt.plot(downConvertedOut)

# plot_fft(downConvertedOut, fs, 'r')

plt.figure("Correlated out")
plt.plot(abs(corrOut))

plt.figure("Depread signal")
plt.plot(despreadSignal, 'b')
plt.plot(np.repeat(avgdespreadSignal,Nb),'r')
plt.plot(np.repeat(avgdespreadSignal1,Nb),'k')

# plot_fft(despreadSignal, fs, 'k')

plt.figure("Correlation outputs")

# plt.subplot(211)
plt.plot(corrSymbol1,'b')
plt.plot(corrSymbol2,'r')
plt.legend(["0","1"])
# plt.plot(np.convolve(corrSymbol1, chipSequence, "same"))

plt.figure("Correlation outputs appended")

plt.subplot(211)
plt.plot((abs(bitCorrTotal1)), 'b')
plt.plot(np.repeat(sentSymbols, Nb), 'k')
# plt.plot(np.repeat(sentSymbols, Nb+Ng), 'k')
# plt.plot((np.cumsum(bitCorrTotal1)), 'b')
# plt.plot((np.cumsum(bitCorrTotal2)), 'r')
plt.subplot(212)
plt.plot((abs(bitCorrTotal2)), 'r')
plt.plot(np.repeat(sentSymbols, Nb), 'k')
# plt.plot(np.repeat(sentSymbols, Nb+Ng), 'k')

# plt.figure("Cumsum outputs appended")

# plt.subplot(211)
# plt.plot(abs(cumsumTotal1), 'b')
# plt.plot(np.repeat(sentSymbols*max(abs(cumsumTotal1)), Nb), 'k')
# plt.plot(np.repeat(sentSymbols*max(abs(cumsumTotal1)), Nb+Ng), 'k')
# plt.plot((np.cumsum(bitCorrTotal1)), 'b')
# plt.plot((np.cumsum(bitCorrTotal2)), 'r')
# plt.subplot(212)
# plt.plot(abs(cumsumTotal2), 'r')
# plt.plot(abs(cumsumTotal3), 'g')
# plt.plot(np.repeat(sentSymbols*max(abs(cumsumTotal1)), Nb+Ng), 'k')
# plt.plot(np.repeat(sentSymbols*0.25*max(abs(cumsumTotal1)), Nb), 'k')
# plt.plot(np.repeat(abs(avgdespreadSignal1)*max(abs(cumsumTotal2)),Nb),'y')

# plt.subplot(212)
# plt.plot(corrSymbol2)
# plt.plot(np.convolve(corrSymbol2, chipSequence, "same"))

fftArray = [inputSignal,downConvertedOut, basebandData, despreadSignal]
fftColorarray = ['y','r','g','b']
fftLegendarray = ['input signal','downconvertedout','basebanddata','despreadSignal']
# plot_multiple_fft(fftArray, fs, fftColorarray, fftLegendarray)

plt.figure("Confidence for detections")
plt.plot(np.repeat(confidenceArray, Nb), 'r')

plt.show()
