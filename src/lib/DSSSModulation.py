import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import max_len_seq
from src.helper.plot_fft import plot_fft
from src.helper.filternew import hpf_filtfilt
from scipy.io import wavfile
import os

fs = 44100
# fs = 48000
ts = 1./fs

fc = 17000

# Ng = 1000
Ng = 0

amplitude = .15

cSeq, _ = max_len_seq(7)
cSeq = 2 * np.array(cSeq) - 1

chipUpsampleFactor = 20

# chipSequence = np.repeat(cSeq, chipUpsampleFactor)
chipSequence = np.array([])
for i in cSeq:
    chipSequence = np.append(chipSequence, i)
    chipSequence = np.append(chipSequence, np.zeros(chipUpsampleFactor-1))

Tc = chipUpsampleFactor*ts

tSinc = np.arange(-4, 4, 0.1)

sinc = np.sinc(tSinc)

sinc /= np.sqrt(np.mean(sinc**2))

chipSequence = np.convolve(chipSequence, sinc)

Nb = len(chipSequence)

d = lambda tDelta, k: np.sin(2*np.pi*float(4+k)*tDelta/Tb)

Tb = Nb*ts

print "Nb :", Nb

fDelta = 2/Tb

# plt.figure("Chip sequence")
# plt.plot(chipSequence)
#
# plt.show()
np.savetxt("Saveddata/chipSequence.txt", chipSequence, newline=", ", fmt="%.6e")

t = np.arange(0, Nb*ts, ts)

numBits = 33

tFullExtra = np.arange(0, 3*Nb*numBits*ts, ts)

Nt = numBits*Nb

symbolSignal = np.array([])

symbolIndexArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
numSymbols = len(symbolIndexArray)

if os.path.isfile("dataBits.npy"):
    dataBits = np.load("dataBits.npy")
else:
    #for 2 bits
    # dataBits = [np.random.randint(2) for i in range(numBits)]
    #for digit with N alphabets
    dataBits = [symbolIndexArray[np.random.randint(numSymbols)] for i in range(numBits)]
    # dataBits = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
    # dataBits = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0]
    dataBitsnpySaveFileName = "databits"+str(dataBits)+".npy"
    np.save("dataBits.npy", dataBits)

print dataBits

upConvertedSignal = np.array([])

spreadSignal = np.array([])

symbolArray = []

symbolFreqArray = []

carrierBit = np.sin(2*np.pi*fc*t)

for i in range(numSymbols):
    symbolData = d(t, symbolIndexArray[i])
    symbolFileName = "Saveddata/symbolData" + str(i)
    np.savetxt(symbolFileName, symbolData, newline=", ", fmt="%.6e")
    symbolArray.append(symbolData)
    symbolFreqArray.append((symbolIndexArray[i]+4)/Tb)
    print "Base band freq of first symbol:", symbolFreqArray[i]

    spreadBitSignal = symbolData * chipSequence

    upConverted = carrierBit * spreadBitSignal

    upConvertedFileName = "Saveddata/upconverted" + str(i)

    np.savetxt(upConvertedFileName, upConverted, newline=", ", fmt="%.6e")

carrierFullExtra = np.sin(2*np.pi*fc*tFullExtra)
np.savetxt("Saveddata/carrierFull", carrierFullExtra, newline=", ", fmt="%.6e")
# symbolIndex1 = 11
# symbolIndex2 = 12
# symbolIndex3 = 13

# symbol1 = d(t, symbolIndex1)
# symbol2 = d(t, symbolIndex2)
# symbol3 = d(t, symbolIndex3)
#
# symbol1Freq = (symbolIndex1+4)/Tb
# symbol2Freq = (symbolIndex2+4)/Tb
# symbol3Freq = (symbolIndex3+4)/Tb
#

carrierBit = np.sin(2*np.pi*fc*t)

for bit in dataBits:
    print bit

    symbol = symbolArray[symbolIndexArray.index(bit)] + 1

    print "Symbol is :", symbolIndexArray.index(bit)

    # if bit == 0:
    #     print "Symbol is :", symbolIndex1
    #     # symbol = symbol1
    #     symbol = symbol1 + 1
    # elif bit == 1:  # if bit is 1:
    #     print "Symbol is :", symbolIndex2
    #     # symbol = symbol2
    #     symbol = symbol2 + 1
    # else:#if bit is 2:
    #     print "Symbol is :", symbolIndex3
    #     # symbol = symbol2
    #     symbol = symbol3 + 1

    symbol *= amplitude

    print "Len og bit spread signal: ", len(symbol)

    symbolSignal = np.append(symbolSignal, symbol)

    spreadBitSignal = symbol*chipSequence

    spreadSignal = np.append(spreadSignal, spreadBitSignal)

    upConverted = carrierBit*spreadBitSignal

    upConvertedSignal = np.append(upConvertedSignal, upConverted)

    if Ng != 0:
        upConvertedSignal = np.append(upConvertedSignal, np.zeros(Ng))

    # upConverted = carrier*symbol

    # upConvertedSignal = np.append(upConvertedSignal, upConverted)

    # spreadBitSignal = upConverted*chipSequence

    # HPFBitsignal = hpf_filtfilt(upConverted, fc, fs, 50, 60)
    #
    # HPFSignal = np.append(HPFSignal, HPFBitsignal)

    # HPFSignal = np.append(HPFSignal, np.zeros(Ng))

    # spreadSignal = np.append(spreadSignal, spreadBitSignal)

    # spreadSignal = np.append(spreadSignal, np.zeros(Ng))

print "Len total spread signal ", len(symbolSignal)
print "Len total spread signal ", Nb*numBits
Ncarrier = len(symbolSignal)

tCarrier = np.arange(0, Ncarrier*ts, ts)
tCarrier = tCarrier[:Ncarrier]
print "len carrier:", len(tCarrier)
carrier = np.sin(2*np.pi*fc*tCarrier)

# spreadSignal = symbolSignal*np.tile(chipSequence, numBits)
#
# upConvertedSignal = spreadSignal*carrier

print "Length of symbol signal:", len(symbolSignal)

# sincInterpolatedchipSequence = np.convolve(chipSequenceTotal, sinc, "full")

# print "Len of sincinterpolated:", len(sincInterpolatedchipSequence)

print "len of upconverted", len(upConvertedSignal)

# upConvertedSignal = np.append(upConvertedSignal,
#                               np.zeros(len(sincInterpolatedchipSequence)-len(upConvertedSignal)))


print "Length of upconverted signal:", len(upConvertedSignal)

# print "Chip length", len(sincInterpolatedchipSequence), len(chipSequenceTotal)

HPFSignal = hpf_filtfilt(spreadSignal, fc, fs, 100, 60)
# HPFSignal = hpf_filtfilt(upConvertedSignal, fc, fs, 50, 120)
# HPFSignal = spreadSignal

print "Len of HPF signal: ", len(HPFSignal)

outFile = "spreadSignal.wav"

finalOutSignal = HPFSignal
# finalOutSignal = upConvertedSignal

print "Length of the final signal:", len(HPFSignal)/float(fs)

wavfile.write(outFile, fs, finalOutSignal)

# plt.figure("Symbols")
#
# plt.subplot(211)
# plt.plot(symbol1)
#
# plt.subplot(212)
# plt.plot(symbol2)

plt.figure("symbol sequence")
plt.plot(symbolSignal)

plot_fft(symbolSignal, fs, 'r')

plt.figure("upconverted")
plt.plot(upConvertedSignal)

plot_fft(upConvertedSignal, fs, 'b')

plt.figure("Spread signal")
plt.plot(spreadSignal)

plot_fft(spreadSignal, fs, 'g')

plt.figure("HPFsignal")
plt.plot(HPFSignal)

plot_fft(HPFSignal, fs, 'y')

# plt.figure("Sinc interpolated output")
# plt.plot(sincInterpolatedchipSequence)

plt.figure("DSSS chip sequence")
plt.plot(chipSequence)

# plt.figure("DSSS Full chip sequence")
# plt.plot(chipSequenceTotal)

plt.show()
