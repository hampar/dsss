import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from src.helper.plot_fft import plot_fft
from scipy.signal import max_len_seq

#number of bits
Nb = 40

#sampling freq
fs = 44.1e3
ts = 1/fs

#samples per symbol
Ns = 1023

#bit duration
Tb = Ns*ts

#bit rate
Rb = 1/Tb
print "Bit rate is : " , Rb

#random set of bits
bits = np.array([np.random.randint(2) for i in range(Nb)])

#shiting from 0,1 to -1,1
bits = 2*bits - 1

#carrier freq
fc=10000

#amplitude
A = 0.15

tFull = np.arange(0, Nb*Ns*ts, ts)
t = np.arange(0, Ns*ts, ts)

messageBPSK = np.array([])

messageDsss = np.array([])

#m sequence length
m = 10

chip = max_len_seq(m)[0]

print "Chip sequence :", chip

#chip rate
Cb = len(chip)*Rb
CNs = Ns/len(chip)
print "Chip rate is : " , Cb

chipSignal = np.repeat(chip, CNs)

print "Length of signal : ", len(t), len(chipSignal)

for bit in bits:
    temp = A*bit*np.cos(2*np.pi*fc*t)
    messageBPSK = np.append(messageBPSK, temp)
    messageDsss = np.append(messageDsss, temp*chipSignal)


plt.figure()
plt.plot(np.repeat(bits,Ns))

plt.figure()
plt.plot(chipSignal)

plt.figure()
plot_fft(messageBPSK,fs,'r')

plt.figure()
plot_fft(messageDsss,fs,'b')

plt.show()


chipCorrelatedOutput = np.correlate(messageDsss, chip, "same")

print bits

plt.figure()
plt.plot(chipCorrelatedOutput)
plt.show()

#using https://www.dsprelated.com/showarticle/1016.php as reference
