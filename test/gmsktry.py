#reference : http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.39.9127&rep=rep1&type=pdf

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy import diff
from src.helper.plot_fft import plot_fft
from scipy.io import wavfile
from src.helper.lowPassFilter import Lpf_filtfilt
from src.helper.upsample import upsample

MODULATION=0
DEMODULATION=1
MODE = MODULATION

#sampling freq
fs = 44.1e3
ts = 1/fs

#number of bits
Nb = 100

#number of samples per bit
Ns = 400

#carrier frequency
fc = 18000

#modulation index
h=0.5

#bit duration
Tb = Ns*ts

#btfactor = 0.3
btFactor = 0.3
#3db BW
W = (btFactor)/Tb

del_f = np.pi/(2*Tb)

print "F delta :", del_f
print "Bit rate : ", 1/Tb

finalSignal = np.array([])

print "The bandwidth for the given Gaussian filter is :",W

if MODE is MODULATION:
    tFull = np.arange(0, Ns*Nb*ts, ts)

    #number of gaussian filter time periods to consider
    numGaussianT = 1

    tG = np.arange(-1*numGaussianT*Ns*ts,numGaussianT*Ns*ts,ts)

    # sigma = np.sqrt(np.log(2))/(2*np.pi*btFactor)

    hG = np.sqrt(2*np.pi/np.log(2))*W*np.exp(-2*((np.pi*W*tG)**2)/np.log(2))
    # hG = np.exp(-1*tG**2/((2*sigma*Tb)**2))/(Tb*sigma*np.sqrt(2*np.pi))

    hG *= np.pi/(2*np.sum(hG))

    #random bits
    bits = np.array([np.random.randint(2) for count in range(Nb)])
    # bits = np.array([1,1,1,1,1,0,0,0,0,0])

    bits = bits*2 - 1

    np.save("sentbits.npy",bits)

    bitSignal = upsample(bits, Ns)[Ns-1:]
    # bitSignal = np.repeat(bits, Ns)
    print "len of bitSignal", len(bitSignal)

    # rectFilter = np.ones(len(tG))

    # bitSignal = signal.lfilter(rectFilter,1,bitSignal)

    # bitSignalIntegrated = np.cumsum(bitSignal)

    gaussianFilteredSignal = signal.lfilter(hG, 1, bitSignal)
    # gaussianFilteredSignal = signal.lfilter(hG, 1, bitSignalIntegrated)
    # gaussianFilteredSignal = np.convolve(bitSignal, hG, "same")

    gaussianFilteredSignal *= (2*np.pi)

    integratedSignal = np.cumsum(gaussianFilteredSignal)

    # integratedSignal *= np.pi/(4*Tb)
    # cosFiltered = np.cos(gaussianFilteredSignal)
    # sinFiltered = np.sin(gaussianFilteredSignal)
    cosFiltered = np.cos(integratedSignal)
    sinFiltered = np.sin(integratedSignal)

    cosCarrier = np.cos(2*np.pi*fc*tFull)
    sinCarrier = np.sin(2*np.pi*fc*tFull)

    finalSignal = cosCarrier*cosFiltered - sinCarrier*sinFiltered

    fileOut = wavfile.write("gmsk18k.wav", int(fs), finalSignal)

    fileTimeDuration = len(finalSignal)*ts

    print "fileTime :", fileTimeDuration

    hilbertOfSignal = signal.hilbert(finalSignal)
    instPhaseOfSignal = np.unwrap(np.angle(hilbertOfSignal))
    instFreqOfSignal = np.diff(instPhaseOfSignal)/(2*np.pi*fs)

    plt.figure()
    plt.plot(bitSignal)

    plt.figure("Gaussian response")
    plt.plot(tG, hG)

    plt.figure("integrated signal")
    # plt.plot(bitSignalIntegrated)
    # plt.plot(integratedSignal)

    plt.figure("Gaussian Filtered signal")
    plt.plot(gaussianFilteredSignal)

    plt.figure()

    plt.subplot(211)
    plt.title("Cos of Fitered")
    plt.plot(cosFiltered)

    plt.subplot(212)
    plt.title("Sin of Fitered")
    plt.plot(sinFiltered)

    plt.figure("Final Signal")
    plt.plot(finalSignal, 'b', bitSignal, 'r')

    plt.figure("Inst Frequency from Hilbert method")
    plt.plot(instFreqOfSignal)

    plt.figure()
    plot_fft(finalSignal,fs,'k')

    plt.show()

elif MODE is DEMODULATION:
    sentBits = np.load("sentbits.npy")
    inputFile = "gmsk18k.wav"
    receivedData,fs1 = wavfile.read(inputFile)

    tFull = np.array(np.arange(0,Ns*Nb*ts,ts))

    # carrierSignalcos = np.cos(2*np.pi*fc*tFull)
    # carrierSignalsin = np.sin(2*np.pi*fc*tFull)
    carrierSignal = np.exp(1j*2*np.pi*fc*tFull)

    # #inPhase component
    # cosComponentData = receivedData*carrierSignalcos
    #
    # #quadrature component
    # sinComponentData = receivedData*carrierSignalsin

    baseBandSignal = receivedData*carrierSignal

    lowPassFilteredBBSignal = Lpf_filtfilt(baseBandSignal,2e3,fs,1000)
    # lowPassFilteredQ = Lpf_filtfilt(sinComponentData,20e3,fs,1000)

    instPhase = np.unwrap(np.angle(lowPassFilteredBBSignal))

    diffOutData = np.diff(instPhase)

    plt.figure("ArcTanOut of Data")
    plt.plot(instPhase)

    plt.figure("Diff of ArcTanData")
    plt.plot(diffOutData,'r',np.repeat(sentBits,Ns),'b')

    plt.show()

