import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from src.helper.plot_fft import plot_fft
from src.helper.upsample import upsample
from scipy.io import wavfile

fs = 44100
ts = 1./fs

Nc = 10
#m sequence length
L = 127

Tc = Nc*ts

Tb = L*Tc

Ns = L*Nc

mSeq = 2*signal.max_len_seq(7)[0] - 1

print "M sequence :",mSeq
numBits = 100

#nrz data points
d = [2*np.random.randint(2)-1 for count in range(numBits)]

data = np.repeat(d,Ns)
# data = upsample(d,Ns)
# data = data[Ns-1:]
prnBit = np.repeat(mSeq, Nc)
# prnBit = upsample(mSeq,Nc)
# prnBit = prnBit[Nc-1:]

prnFullData = np.array([])
for i in range(numBits):
    prnFullData = np.append(prnFullData, data[i*Ns:(i+1)*Ns]*prnBit)

plt.figure("PRN bit")
plt.plot(prnBit)

print "Length of data signal :",len(data)
print "Length of prn signal :",len(prnFullData)

# dsssModulatedSignal = np.convolve(data, prnBit, "same")
dsssModulatedSignal = prnFullData

numGaussianT = 1
btFactor = 0.3
h=0.5
fc=18000

W = btFactor/Tc

tF = np.arange(0,Ns*numBits*ts-ts,ts)

tG = np.arange(-1 * numGaussianT * Nc * ts, numGaussianT * Nc * ts, ts)

print "Bandwidth :", W
print "Bit period:", Tb
print "Bit rate:",1/Tb
print len(tG)

sigma = np.sqrt(np.log(2)) / (2 * np.pi * btFactor)

hG = np.sqrt(2 * np.pi / np.log(2)) * W * np.exp(-2 * ((np.pi * W * tG) ** 2) / np.log(2))

hG *= np.pi/(2*sum(hG))

# integratedSignal = np.cumsum(dsssModulatedSignal)

# gaussianFilteredSignal = signal.lfilter(hG,1,integratedSignal)
# gaussianFilteredSignal = signal.filtfilt(hG, 1, dsssModulatedSignal)

gaussianFilteredSignal = signal.lfilter(hG, 1, dsssModulatedSignal)

# gaussianFilteredSignal *= h/(2*Tc)
# gaussianFilteredSignal *= (2*np.pi)

integratedSignal = np.cumsum(gaussianFilteredSignal)

inPhaseComponent = np.cos(integratedSignal)

quadratureComponent = np.sin(integratedSignal)

carrierInPhaseComponent = np.cos(2*np.pi*fc*tF)
carrierQuadPhaseComponent = np.sin(2*np.pi*fc*tF)

print "ccaarrier in phase length :", len(carrierInPhaseComponent)

finalSignal = carrierInPhaseComponent*inPhaseComponent - carrierQuadPhaseComponent*quadratureComponent

fileOut = wavfile.write("gmskDSSS.wav", fs, finalSignal)

f,psdFinalSignal = signal.welch(finalSignal,fs,nperseg=64)

fData,psdData = signal.welch(data, fs, nperseg=64)

plt.figure("data Signal")
plt.plot(data)

# plt.figure("Spectrum of sent data")
plot_fft(data, fs, "r")

# plt.figure("Spectrum of spread signal")
plot_fft(dsssModulatedSignal, fs ,'b')

# plt.figure("PRN sequence")
# plt.plot(prnFullData)
#
# plt.figure("DSSS modulated signal")
# plt.plot(dsssModulatedSignal)

# plt.figure("Gaussian filtered signal")
# plt.plot(gaussianFilteredSignal)
#
# plt.figure("Gaussian filter response")
# plt.plot(hG)

# plt.figure("Integrated signal")
# plt.plot(integratedSignal)
#
# plt.figure("baseband component")
#
# plt.subplot(211)
# plt.plot(inPhaseComponent)
#
# plt.subplot(212)
# plt.plot(quadratureComponent)

plt.figure("Final Signal")
plt.plot(finalSignal)

plt.figure("Final signal PSD")
plt.plot(f,psdFinalSignal)

plt.figure("Data PSD")
plt.plot(fData,psdData)

plt.figure("Final Signal Spectrum")
plot_fft(finalSignal,fs,'b')
plt.show()
