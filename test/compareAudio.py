import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.io import wavfile

fs = 44.1e3
ts = 1/fs

numSamples = 120*2048

chirpFileGenerated = "/home/harshaampar/chirp14kto20k.wav"

wavIn = wavfile.read(chirpFileGenerated)[1]

print wavIn

tMax = numSamples/fs

t = np.arange(ts,tMax,ts)

fStart = 14e3
fEnd = 20e3

chirp14to20k = signal.chirp(t,fStart, t[-1], fEnd)

print "Length of generated chirp", min(chirp14to20k)
print "Length of Audacity generated chirp", min(wavIn)

plt.figure()
plt.plot(chirp14to20k*2**14-wavIn)
plt.show()