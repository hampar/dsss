import numpy as np
from scipy.io import wavfile

fs=44.1e3
ts=1/fs

f0=17000
f1=17500
f2=18000

numSamplesPerBit = 1000

numBits = 200

t = np.arange(0, numSamplesPerBit*ts, ts)

data = np.array([])

for i in range(numBits):
    if np.random.random() > 0.5:
        data = np.append(data, np.sin(2*np.pi*f0*t))#+np.sin(2*np.pi*f1*t))
    else:
        data = np.append(data, np.sin(2*np.pi*f1*t))#+np.sin(2*np.pi*f2*t))

saveFile = wavfile.write("out.wav", int(fs), data)

