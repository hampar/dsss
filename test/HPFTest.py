from scipy.signal import firwin, freqz
import matplotlib.pyplot as plt
import numpy as np

N = 421
cutoffFreq = 16500
fs = 44100.

if __name__ == '__main__':
    taps = firwin(N, 2*cutoffFreq/ fs, window='hamming', pass_zero=False)

    np.savetxt("taps421.txt", taps, newline=" ")

    w, h = freqz(taps)

    plt.figure()
    plt.plot(w, 20 * np.log10(abs(h)), 'b')
    plt.ylabel('Amplitude [dB]', color='b')
    plt.xlabel('Frequency [rad/sample]')
    plt.show()